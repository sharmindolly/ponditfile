
   



(function($) {
    "use strict";
     /*last word */
    $('.success2-content, .virtual-course .title, .s-right p').each(function(index, element) {
            var heading = $(element);
            var word_array, last_word, first_part;
        
            word_array = heading.html().split(/\s+/); // split on spaces
            last_word = word_array.pop();             // pop the last word
            first_part = word_array.join(' ');        // rejoin the first words together
        
            heading.html([first_part, ' <span class="last-word">', last_word, '</span>'].join(''));
        });




    $(document).ready(function() {
        $('.details-media').venobox();
    });

    $(".dropdown-toggle").on("hover",function(){
        $(this).siblings(".dropdown-menu").toggleClass("show")
    })

    /*sticky navbar js here*/
    var mn = $(".navbar");
    var mns = "header-fixed";
    var hdr = $('.header-top').height() + 30;
    var mnh = $(".navbar").height();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > hdr) {
            mn.addClass(mns);
            $('.header-top').css("margin-bottom", mnh);
            mn.css("transition"," all 0.7s ease 0s")

        } else {
            mn.removeClass(mns);
            $('.header-top').css("margin-bottom", 0);
            mn.css("transition"," all 0.5s ease 0s")
        }
    });
        var iScrollPos = 0;
        $(window).on("scroll",function () {
            var iCurScrollPos = $(this).scrollTop();
            if (iCurScrollPos > iScrollPos) {
                mn.addClass("scrollDown")
            } else {
               mn.removeClass("scrollDown")
            }
            iScrollPos = iCurScrollPos;
        });


        var drh = $("#details-top").height() + $("header").height();
        $(window).on("scroll", function() {
        if ($(this).scrollTop() > drh) {
            $(".d-right .fixed-content").addClass("d-right-fixed");
        } else if($(this).scrollTop() < $("#details-top").height()){
            $(".d-right .fixed-content").removeClass("d-right-fixed");
        }
    });

    function scrollTo(target) {
        $('html').animate({
            scrollTop: $(target).offset().top
        }, 1000);
        return false;
    }

    $('#success-story .owl-carousel').owlCarousel({
        center: true,
        autoplay: true,
        autoplayTimeout: 8000,
        margin: 10,
        loop: true,
        items: 1
    })

    $('#partner .owl-carousel').owlCarousel({
        margin: 30,
        autoplay: true,
        autoplayTimeout: 5000,
        nav: true,
        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            1000: {
                items: 6,
                nav: true,
            }
        }
    })

    $('#success-story .owl-dots').css("display", "inline-flex");
    $('#success-story .owl-dots').wrap('<div class="text-center"></div>');

    $(document).on("ready", function() {
        $('.pondit-media').venobox({
            framewidth: '', // default: ''
            frameheight: '', // default: ''
            spinner: 'cube-grid',
            overlayClose: 'true',
            autoplay: 'true'
        });

        $(".vbox-content iframe").on("load", function() {
            alert("hello world")
        });
    })


    /*scroll it for section navigation*/
        $(function() {
            $.scrollIt();
        });
        // go top js

        var bh = $("header").height();
        $(".go-top").hide();

        $(window).on("scroll", function() {
            if ($(this).scrollTop() > bh) {
                $(".go-top").show();
            } else {
                $(".go-top").hide();
            }
        });

    /*payment jquery goes here*/
        $(document).ready(function() {
            $(".instruction").hide();
            $(".step1-text").show();
            $(".pay-same").hide();
            $(".pay-step1").show();
            $(".payment-details").hide();
        })
        $(".next").on("click", function() {
            var input_length = $("#phone-number").val().length;
            if (input_length !== 0) {
                $(".pay-same").hide();
                $(".pay-step2").show();
                $(".d-color-2").addClass("active");
                $(".instruction").hide();
                $(".step2-text").show();
            }

        })
        $(".finish").on("click", function() {
            $(".pay-same").hide();
            $(".pay-step3").show();
            $(".d-color-3").addClass("active");
            $(".instruction").hide();
            $(".step3-text").show();
        });


        $(".previous").on("click", function() {
            $(".pay-same").hide();
            $(".pay-step1").show();
            $(".d-color-2").removeClass("active");
            $(".instruction").hide();
            $(".step1-text").show();
        })
        $(".bkash-btn").on("click", function() {
            $(".paymentmethods").removeClass("d-flex");
            $(".paymentmethods").addClass("d-none");
            $(".payment-details").hide();
            $("#bkashDetails").show();
        })
        $(".rocket-btn").on("click", function() {
            $(".paymentmethods").removeClass("d-flex");
            $(".paymentmethods").addClass("d-none");
            $(".payment-details").hide();
            $("#rocketDetails").show();
        })
        $(".ific-btn").on("click", function() {
            $(".paymentmethods").removeClass("d-flex");
            $(".paymentmethods").addClass("d-none");
            $(".payment-details").hide();
            $("#fastSecurityDetails").show();
        })
        $(".another-gateway").on("click", function() {
            $(".paymentmethods").removeClass("d-none");
            $(".paymentmethods").addClass("d-flex");
            $(".payment-details input").val("");
            $(".payment-details").hide();
        })


    /*input label animation style*/
        $(".form-control").on("focus", function() {
            $(this).siblings("label").addClass("active")
        })

        $(window).on("load", function() {
            $("input").val("")
        })
        $(".form-control").on("blur", function() {
            var input_length = this.value.length;
            if (input_length > 0) {
                $(this).siblings("label").addClass("active")
            } else {
                $(this).siblings("label").removeClass("active")
            }
        })

        $(".collapse-all").on("click",function(){
            $(".expand").removeClass("d-none");
            $(".tab-pane .collapse").removeClass("show");
            $(".tab-pane .card-header").addClass("collapsed");
            $(".collapse-all").addClass("d-none");
        })

        $(".expand").on("click",function(){
            $(this).addClass("d-none");
            $(".tab-pane .collapse").addClass("show");
            $(".tab-pane .card-header").removeClass("collapsed");;
            $(".collapse-all").removeClass("d-none");
        });

        $(".course-address").hide();
        $(".course-address.show").show();
        $(".course-type label").on("click",function(){
            $(".course-address").hide();
            $(this).siblings("p").show()
        })

    /*rating plugin initialization*/
        $("#rateYo").rateYo({
            rating: 3.6
          });
        $("#rateYo").rateYo("option", "starWidth", "25px");

        $("#rateYo").on("click",function(){
            var $rateYo = $("#rateYo").rateYo();

            var rating = $rateYo.rateYo("rating");

            $("#rating").text(rating);
        })


    /*course breadcrumbs text limit here*/
        var elem = $(".course-name");         // The element or elements with the text to hide
        var limit = 35;        // The number of characters to show
        var str = elem.html();    // Getting the text
        var strtemp = str.substr(0,limit);   // Get the visible part of the string
        str = strtemp + '<span class="hidden-text">' + " ..." + '</span>';  // Recompose the string with the span tag wrapped around the hidden part of it
        elem.html(str);       // Write the string to the DOM 

}(jQuery));

$(document).ready(function() {
    $("input[type='tel'").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});


